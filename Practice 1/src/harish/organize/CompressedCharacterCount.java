package harish.organize;
/*Keep track of string count and compress and rewrite as follows:
* aabcccccaaa -> a2bc5a3
*/
public class CompressedCharacterCount {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String stringToConsider = "aaaaaaaaabbbbbbbbaabbabababccccccccc";
		String compressedString = "";
		char currentChar = stringToConsider.charAt(0);
		int currentCount = 1;
		for(int i = 1;i<=stringToConsider.length();i++) {
			if(i==stringToConsider.length()) {
				compressedString = compressedString+currentChar+currentCount;
			}
			else {
			if(stringToConsider.charAt(i)==currentChar) {
				currentCount+=1;
			}
			else {
				compressedString = compressedString+currentChar+currentCount;
				currentChar = stringToConsider.charAt(i);
				currentCount = 1;
			}
		}
		}
		System.out.println(compressedString);
	}

}
