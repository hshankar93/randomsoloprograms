package harish.organize;

public class PartitionList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Node firstNode = new Node(5);
		firstNode.appendToTail(10);
		firstNode.appendToTail(12);
		firstNode.appendToTail(15);
		firstNode.appendToTail(17);
		firstNode.appendToTail(19);
		firstNode.appendToTail(20);
		firstNode.appendToTail(23);
		int centerAround = 15;
		Node listThatFollows = null;
		Node tailList1 = null;
		Node tailList2 = null;
		Node listBefore = null;
		while(firstNode!=null) {
			Node nextNode = firstNode.next;
			firstNode.next = null;
			if(firstNode.data<centerAround) {
				if(listBefore==null) {
					listBefore = firstNode;
					tailList1 = listBefore;
				}
				else {
					tailList1.next = firstNode;
					tailList1 = firstNode;
				}
			}
			else {
				if(listThatFollows==null) {
					listThatFollows = firstNode;
					tailList2 = listThatFollows;
				}
				else {
					tailList2.next = firstNode;
					tailList2 = firstNode;
				}
				firstNode = firstNode.next;
				}

			firstNode = nextNode;
			}
		tailList1.next = listThatFollows;
		Node centeredNode = listBefore;
		centeredNode.displayList();
		}
		
	}

