package harish.organize;

public class Student {
	String Name;
	int age;
	char gender;
	
	public Student() {
		System.out.println("Basic Constructor Evoked");
	}
	
	public Student(int age) {
		this();
		System.out.println("Constructor with age field Evoked");
	}
	
	public Student(char gender) {
		this(10);
		System.out.println("Constructor with gender field Evoked");
	}
	public void ShowMyValues() {
		System.out.println(this.Name+String.valueOf(this.age)+this.gender);
	}
}
