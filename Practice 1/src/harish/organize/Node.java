package harish.organize;

public class Node {
	int data;
	Node next = null;
	
	public Node(int d) {
		this.data = d;
	}
	
	public void appendToTail(int d) {
		Node newEnd = new Node(d);
		Node currEnd = this;
		while(currEnd.next!=null)
			currEnd = currEnd.next;
		currEnd.next = newEnd;
		
	}
	
	public void displayList() {
		Node base = this;
		int i = 0;
		while(base.next!=null) {
			System.out.println("Pointer #"+i+" = "+base.data);
			base = base.next;
			i++;
		}

		System.out.println("Pointer #"+i+" = "+base.data);
	}
	
	public Node deleteNode(Node head, int d) {
		Node iteratingNode = head;
		Node NodeToReturn = head;
		if(this.data != d) {
			while(iteratingNode.next != null) {
				if(iteratingNode.next.data == d) {
					iteratingNode.next = iteratingNode.next.next;
					NodeToReturn = iteratingNode.next;
					iteratingNode = iteratingNode.next;
				}
				else {
					iteratingNode = iteratingNode.next;
				}
			}
		}
		else {
			return head.next;
		}
			
		return NodeToReturn;
	}
	
	public int returnReverseNumber() {
		String temp = "";
		Node new1 = this;
		while(new1!=null) {
			temp = temp+Integer.toString((new1.data));
			new1 = new1.next;
		}
		temp = new StringBuilder(temp).reverse().toString();
		return Integer.parseInt(temp);
	}
	
	public static void main(String[] args) {
		Node firstNode = new Node(5);
		firstNode.appendToTail(10);
		firstNode.appendToTail(15);
		firstNode.appendToTail(20);
		firstNode.displayList();
		Node deletedNode = firstNode.deleteNode(firstNode,5);
		System.out.println("Node deleted");
		deletedNode.displayList();
	}

}
