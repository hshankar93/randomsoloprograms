package harish.organize;
//Replace all spaces in a string with %20
public class ReplaceSpaceWith20 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String input = "Mr John Smith      ";
		// Output should be "Mr%20John%20Smith"
		int trueLength = 13;
		String trueString = "";
		String[] words = input.split(" ");
		for(int i = 0;i<words.length;i++) {
			if(i<words.length-1)
				trueString += words[i]+"%20";
			else
				trueString += words[i];
		}
		System.out.println(trueString);

	}

}
