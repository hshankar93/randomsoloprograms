package harish.organize;

public class StockPrices {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] stockPricesYesterday = new int[]{10, 7, 5, 8, 11, 9};
		System.out.println("Maximum profit = "+Integer.toString(getMaxProfit(stockPricesYesterday)));
		stockPricesYesterday = new int[]{11,5,10,4,12,2};
		System.out.println("Maximum profit = "+Integer.toString(getMaxProfit(stockPricesYesterday)));
	}
	
	public static int getMaxProfit ( int[] stockPricesYesterday) {
		if(stockPricesYesterday.length>1) {
			int minPrice = stockPricesYesterday[0];
			int maxProfit = stockPricesYesterday[1]-stockPricesYesterday[0];
			for(int i = 1;i<stockPricesYesterday.length;i++) {
				int someIntermediateProfit = stockPricesYesterday[i]-minPrice;
				maxProfit = Math.max(maxProfit, someIntermediateProfit);
				minPrice = Math.min(minPrice, stockPricesYesterday[i]);
			}
			return maxProfit;
		}
		else
		return 0;
	}

}
