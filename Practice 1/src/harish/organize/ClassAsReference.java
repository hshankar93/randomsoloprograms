package harish.organize;
public class ClassAsReference {
	
	public static void CallFunction(){
		System.out.println("First Statement");
		return;
	}
	
	public static void main(String[] args){
	Flight boeing1 = new Flight(10,"boeing111");
	Flight boeing2 = new Flight(20,"boeing222");
	boeing1.name = "Harish";
	boeing2.name = "Shankar";
	System.out.println(boeing1.name+boeing2.name);
	CallFunction();
}
}
