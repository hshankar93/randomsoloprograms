package harish.organize;
// Check if any 2 strings are permutations.

import java.util.Arrays;

public class StringPermutation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String a ="harish";
		String b = "hsirah";
		System.out.print(StringPerm(a,b));

	}
	
	public static boolean StringPerm(String a, String b) {
		char[] a1 = a.toCharArray();
		char[] b1 = b.toCharArray();
		Arrays.sort(a1);
		Arrays.sort(b1);
		return Arrays.equals(a1, b1);
		
	}

}
