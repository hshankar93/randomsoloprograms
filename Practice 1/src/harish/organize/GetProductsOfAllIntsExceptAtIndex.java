package harish.organize;

import java.util.Arrays;

public class GetProductsOfAllIntsExceptAtIndex {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] givenArray = new int[]{1,7,3,4};
		int[] productArray = new int[givenArray.length];
		for(int i=0;i<givenArray.length;i++)
			productArray[i]=1;
		int forwardMult = 1;
		for(int i = 0;i<givenArray.length;i++) {
			productArray[i] = forwardMult;
			forwardMult *= givenArray[i];
		}
		int backwardMult = 1;
		for(int i = givenArray.length-1;i>=0;i--) {
			productArray[i] *= backwardMult;
			backwardMult *= givenArray[i];
		}
		System.out.println(Arrays.toString(productArray));

	}

}
