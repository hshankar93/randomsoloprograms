package harish.organize;

import java.util.HashSet;

public class RemoveLLDuplicates {
	public static void main(String[] args) {
		Node firstNode = new Node(5);
		firstNode.appendToTail(10);
		firstNode.appendToTail(10);
		firstNode.appendToTail(15);
		firstNode.appendToTail(15);
		firstNode.appendToTail(15);
		firstNode.appendToTail(20);
		firstNode.appendToTail(20);
		firstNode.displayList();
		remove(firstNode);
		firstNode.displayList();
	}
	public static void remove(Node node) {
		HashSet<Integer> uniqueSet = new HashSet<Integer>();
		Node previous = null;
		while(node!=null) {
			if(uniqueSet.contains(node.data)) {
				previous.next = node.next;
			}
			else {
				uniqueSet.add(node.data);
				previous = node;
			}
			node = node.next;
		}
		
	}
}
