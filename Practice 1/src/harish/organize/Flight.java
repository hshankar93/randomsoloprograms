package harish.organize;

public class Flight {
	int passengers;
	String name;
	
	public Flight(int passengers, String name){
		this.passengers = passengers;
		this.name = name;
		System.out.println("Constructor invoked");
	}

	public void incrementPassengerCount(){
		this.passengers+=1;
	}
}
