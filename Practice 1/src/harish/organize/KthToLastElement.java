package harish.organize;

public class KthToLastElement {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Node firstNode = new Node(5);
		firstNode.appendToTail(10);
		firstNode.appendToTail(10);
		firstNode.appendToTail(15);
		firstNode.appendToTail(15);
		firstNode.appendToTail(15);
		firstNode.appendToTail(20);
		firstNode.appendToTail(20);
		printKthElement(firstNode,6);
	}
	
	public static void printKthElement(Node node,int k) {
		Node copyNode = node;
		int lengthOfList=0;
		while(node!=null) {
			lengthOfList++;
			node = node.next;
		}
		int lengthToGoTill = lengthOfList - k;
		int counter = 0;
		while(copyNode!=null) {
			if(counter==lengthToGoTill) {
				System.out.println("Kth element from the back is = "+copyNode.data);
				break;
			}
			else {
				counter++;
				copyNode = copyNode.next;
			}
				
		}
	}

}
