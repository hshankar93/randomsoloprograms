package harish.organize;

public class ClassInheritance extends exampleClass{

	ClassInheritance(int x) {
		super(x);
		//System.out.println("Always need to call super class constructor before doing anything within current constructor");
		// TODO Auto-generated constructor stub
	}
	
	@Override
	void SayHello() {
		System.out.println("Hello from the same side!");
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ClassInheritance C = new ClassInheritance(10);
		C.SayHello();
	}

}
