package harish.organize;

// Rotate a picture represented as an integer array nxn
public class RotatePixelImage {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[][] pixelMap = new int[10][];
		swapBy90(pixelMap,10);
	}
	public static void swapBy90(int[][] pixelMap,int size) {
		// Idea is to treat the nxn matrix as outer layers going inwards.
		for(int layers=0;layers<size/2;layers++) {
			for(int elem=layers;elem<size-1-layers;elem++) {
				int[] temp = new int[size-1-layers];
				temp[elem] = pixelMap[layers][elem];
				//Take from left row
				pixelMap[layers][elem]=pixelMap[size-1-elem][layers];
				
				//Take from bottom
				pixelMap[layers][elem]=pixelMap[size-1-elem][layers];
				
			}
		}
		
	}
}
