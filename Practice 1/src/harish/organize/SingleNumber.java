package harish.organize;

import java.util.Arrays;

/*Given an array of integers, every element appears twice except for one. Find that single one.
 * Note: Your algorithm should have a linear runtime complexity. 
 * Could you implement it without using extra memory?
 */
public class SingleNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] n = {2,5,1,3,66,1,3,66,5};
		int singleOccurence = singleNumber(n);
		System.out.println(singleOccurence);
	}
	public static int singleNumber(int[] n) {
		int onlyOnce=0;
		/*Arrays.sort(n);
		for(int i=1;i<n.length;i+=2) {
			if(n[i]!=n[i-1]) {
				if(n[i]!=n[i+1]) {
					onlyOnce = n[i];
					break;
				}
				else {
					onlyOnce = n[i-1];
					break;
				}
			}
		}*/
		for(int i=0;i<n.length;i++)
			onlyOnce^=n[i];
		return onlyOnce;
	}
}
