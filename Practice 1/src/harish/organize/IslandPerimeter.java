package harish.organize;

import java.util.ArrayList;
import java.util.Collections;

public class IslandPerimeter {
	public static void main(String[] args) {
		int[][] grid = new int[100][100];
		grid[0][1]=grid[1][0]=grid[1][1]=grid[1][2]=grid[2][1]=grid[3][0]=grid[3][1]=1;
		islandPerimeter(grid);
	}
	
	public static void islandPerimeter(int[][] grid) {
		int sidesToCount = 0;
		int sidesToNegate = 0;
		int dimension = grid.length;
		ArrayList<Integer> highlightedBoxes = new ArrayList<Integer>();
		// Keep track of sides to negate since islands next to each other give 2 sides lesser to perimeter
		for(int i=0;i<dimension;i++) {
			int yDimension = grid[i].length;
			for(int j=0;j<yDimension;j++) {
				if(grid[i][j]==1) {
					highlightedBoxes.add(BoxNumber(i,j));
					sidesToCount += 4; 
					//Check if next box to right is also island
					if(j<yDimension-1 && grid[i][j+1]==1)
						sidesToNegate += 2;
					//Check if next box below is also island
					if(i<dimension-1 && grid[i+1][j]==1)
						sidesToNegate += 2;
				}
					
			}
		}
		Collections.sort(highlightedBoxes);
		System.out.println("The islands are in boxes "+highlightedBoxes.toString());
		int perimeter = sidesToCount-sidesToNegate;
		System.out.println("Island perimeter = "+Integer.toString(perimeter));
		
		
	}
	public static int BoxNumber(int i, int j) {
		int boxNum = i*100+j;
		return boxNum;
	}

}
