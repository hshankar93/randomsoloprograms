package harish.organize;

public class SumInReverseList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Node firstNode = new Node(7);
		firstNode.appendToTail(1);
		firstNode.appendToTail(6);
		Node secondNode = new Node(5);
		secondNode.appendToTail(9);
		secondNode.appendToTail(2);
		int integer1 = firstNode.returnReverseNumber();
		int integer2 = secondNode.returnReverseNumber();
		int sum = integer1+integer2;
		Node resultNode = new Node(sum%10);
		sum/=10;
		while(sum/10!=0) {
			resultNode.appendToTail(sum%10);
			sum/=10;
		}
		resultNode.appendToTail(sum%10);
		resultNode.displayList();
	}

}
